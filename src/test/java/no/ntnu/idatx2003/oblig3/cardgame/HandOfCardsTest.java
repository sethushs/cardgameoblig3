package no.ntnu.idatx2003.oblig3.cardgame;

import static org.junit.jupiter.api.Assertions.*;



import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

class HandOfCardsTest {

  @Test
  void testHasFlush_WithFlush() {
    // Arrange
    HandOfCards hand = new HandOfCards(Arrays.asList(
      new PlayingCard('H', 2),
      new PlayingCard('H', 3),
      new PlayingCard('H', 4),
      new PlayingCard('H', 5),
      new PlayingCard('H', 6)
    ));

    // Act
    boolean result = hand.hasFlush();

    // Assert
    assertTrue(result, "Hand should have a flush");
  }

  @Test
  void testHasFlush_WithoutFlush() {
    // Arrange
    HandOfCards hand = new HandOfCards(Arrays.asList(
      new PlayingCard('H', 2),
      new PlayingCard('S', 3),
      new PlayingCard('D', 4),
      new PlayingCard('C', 5),
      new PlayingCard('H', 6)
    ));

    // Act
    boolean result = hand.hasFlush();

    // Assert
    assertFalse(result, "Hand should not have a flush");
  }
  @Test
  void testSumOfFaces() {
    // Arrange
    HandOfCards hand = new HandOfCards(Arrays.asList(
      new PlayingCard('H', 1), // Ess som 1
      new PlayingCard('S', 11), // Knekt som 11
      new PlayingCard('D', 12), // Dronning som 12
      new PlayingCard('C', 13)  // Konge som 13
    ));

    // Act
    int sum = hand.sumOfFaces();

    // Assert
    assertEquals(37, sum, "Sum of faces should be 37");
  }
  @Test
  void testCardsOfHearts() {
    // Arrange
    HandOfCards hand = new HandOfCards(Arrays.asList(
      new PlayingCard('H', 2),
      new PlayingCard('H', 10),
      new PlayingCard('S', 5)
    ));

    // Act
    String hearts = hand.cardsOfHearts();

    // Assert
    assertTrue(hearts.contains("H2") && hearts.contains("H10"), "Should contain H2 and H10");
  }
  @Test
  void testHasQueenOfSpades() {
    // Arrange
    HandOfCards hand = new HandOfCards(Arrays.asList(
      new PlayingCard('S', 12),
      new PlayingCard('H', 7),
      new PlayingCard('D', 5)
    ));

    // Act
    boolean hasQueenOfSpades = hand.hasQueenOfSpades();

    // Assert
    assertTrue(hasQueenOfSpades, "Hand should have the Queen of Spades");
  }

}
