package no.ntnu.idatx2003.oblig3.cardgame;

import static org.junit.jupiter.api.Assertions.*;



import org.junit.jupiter.api.Test;



class PlayingCardTest {

  @Test
  void constructorShouldInitializeSuitAndFaceCorrectly() {
    // Arrange
    char expectedSuit = 'H'; // Hjerter
    int expectedFace = 12; // Dronning

    // Act
    PlayingCard card = new PlayingCard(expectedSuit, expectedFace);

    // Assert
    assertAll("card properties",
      () -> assertEquals(expectedSuit, card.getSuit(), "Suit should match constructor argument."),
      () -> assertEquals(expectedFace, card.getFace(), "Face should match constructor argument.")
    );
  }

  @Test
  void getAsStringShouldReturnCorrectStringRepresentation() {
    // Arrange
    char suit = 'S'; // Spar
    int face = 1; // Ess
    PlayingCard card = new PlayingCard(suit, face);
    String expectedString = "S1";

    // Act
    String cardString = card.getAsString();

    // Assert
    assertEquals(expectedString, cardString, "String representation should match 'S1' for Ace of Spades.");
  }
}
