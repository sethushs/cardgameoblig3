package no.ntnu.idatx2003.oblig3.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

  private DeckOfCards deck;

  @BeforeEach
  void setUp() {
    // Arrange
    deck = new DeckOfCards();
  }

  @Test
  void deckShouldBeInitializedWith52Cards() {
    // Act
    int deckSize = deck.cardsLeft();

    // Assert
    assertEquals(52, deckSize, "A new deck should contain 52 cards.");
  }

  @Test
  void dealingFiveCardsShouldReduceDeckSizeByFive() {
    // Arrange - Deck is initialized in setUp()
    int originalSize = deck.cardsLeft();

    // Act
    deck.dealHand(5);
    int newSize = deck.cardsLeft();

    // Assert
    assertEquals(originalSize - 5, newSize, "Dealing five cards should reduce deck size by 5.");
  }

  @Test
  void shuffleShouldNotChangeDeckSize() {
    // Arrange - Deck is initialized in setUp()
    int originalSize = deck.cardsLeft();

    // Act
    deck.shuffle();
    int newSize = deck.cardsLeft();

    // Assert
    assertEquals(originalSize, newSize, "Shuffling should not change the number of cards in the deck.");
  }

  @Test
  void dealFlushHandShouldReturnCorrectNumberOfCards() {
    // Arrange
    int flushSize = 5;
    // Act
    List<PlayingCard> flushHand = deck.dealFlushHand(flushSize);

    // Assert
    assertEquals(flushSize, flushHand.size(), "Flush hand should contain the requested number of cards.");
  }

}
