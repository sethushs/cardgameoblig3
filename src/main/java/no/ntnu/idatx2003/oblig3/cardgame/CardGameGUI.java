package no.ntnu.idatx2003.oblig3.cardgame;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 * The CardGameGUI class provides a GUI for a simple card game.
 * It allows the user to deal a hand of cards, check the hand for specific combinations,
 * and force a flush hand from the remaining cards in the deck.
 */
public class CardGameGUI extends Application {

  /** The deck of cards used in the game. */
  private DeckOfCards deck = new DeckOfCards();

  /** The current hand of cards dealt. */
  private HandOfCards currentHand;

  /** The text area for displaying card information and game results. */
  private TextArea cardDisplay = new TextArea();

  /**
   * The start method initializes and displays the graphical user interface.
   * @param cardgame The primary stage for the JavaFX application.
   */
  @Override
  public void start(Stage cardgame) {
    cardgame.setTitle("Card Game");
    deck.shuffle();

    Button dealButton = new Button("Deal Hand");
    dealButton.setOnAction(e -> {
      currentHand = new HandOfCards(deck.dealHand(5));
      cardDisplay.setText("Dealt Hand:\n" + currentHand.displayHand());
    });

    Button checkHandButton = new Button("Check Hand");
    checkHandButton.setOnAction(e -> {
      if (currentHand != null) {
        checkHand();
      } else {
        cardDisplay.appendText("\nPlease deal a hand first.\n");
      }
    });

    Button forceFlushButton = new Button("Force Flush");
    forceFlushButton.setOnAction(e -> {
      if (deck.cardsLeft() < 5) {
        cardDisplay.setText("Not enough cards in the deck for a flush. Please restart the game.\n");
        return;
      }
      currentHand = new HandOfCards(deck.dealFlushHand(5));
      cardDisplay.setText("Forced Flush Hand:\n" + currentHand.displayHand());
    });

    VBox layout = new VBox(10, cardDisplay, dealButton, checkHandButton, forceFlushButton);
    layout.setAlignment(Pos.CENTER);
    layout.setPadding(new Insets(20));

    Scene scene = new Scene(layout, 400, 300);
    cardgame.setScene(scene);
    cardgame.show();
  }

  /**
   * Checks the current hand for specific combinations and displays the results.
   */
  private void checkHand() {
    StringBuilder results = new StringBuilder("\nChecking Hand:\n");
    results.append("Sum of Faces: ").append(currentHand.sumOfFaces()).append("\n");
    results.append("Has Flush: ").append(currentHand.hasFlush() ? "Yes" : "No").append("\n");
    results.append("Has Queen of Spades: ").append(currentHand.hasQueenOfSpades() ? "Yes" : "No").append("\n");
    results.append("Cards of Hearts: ").append(currentHand.cardsOfHearts().isEmpty() ? "No Hearts" : currentHand.cardsOfHearts());

    cardDisplay.appendText(results.toString());
  }

  /**
   * The main method to launch the JavaFX application.
   * @param args The command line arguments.
   */
  public static void main(String[] args) {
    launch(args);
  }
}
