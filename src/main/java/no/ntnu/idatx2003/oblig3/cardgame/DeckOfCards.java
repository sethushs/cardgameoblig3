package no.ntnu.idatx2003.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
/**
 * The DeckOfCards class represents a standard deck of playing cards.
 * It allows for shuffling the deck, dealing hands of cards, and dealing
 * hands of cards with a specified flush (cards of the same suit).
 */
public class DeckOfCards {

  /** The list of cards in the deck. */
  private List<PlayingCard> cards = new ArrayList<>();

  /** The array of suits in a standard deck of cards. */
  private static final char[] SUITS = {'S', 'H', 'D', 'C'};

  /** The random number generator for shuffling and dealing cards. */
  private Random random = new Random();

  /**
   * Constructs a new deck of cards with all 52 standard playing cards.
   * Each suit has cards numbered from 1 to 13.
   */
  public DeckOfCards() {
    for (char suit : SUITS) {
      for (int face = 1; face <= 13; face++) {
        cards.add(new PlayingCard(suit, face));
      }
    }
  }

  /**
   * Shuffles the cards in the deck randomly.
   */
  public void shuffle() {
    Collections.shuffle(cards, random);
  }

  /**
   * Deals a hand of cards from the deck.
   * @param n The number of cards to deal.
   * @return A list of PlayingCard objects representing the dealt hand.
   */
  public List<PlayingCard> dealHand(int n) {
    List<PlayingCard> hand = new ArrayList<>();
    for (int i = 0; i < n && !cards.isEmpty(); i++) {
      hand.add(cards.remove(random.nextInt(cards.size())));
    }
    return hand;
  }

  /**
   * Deals a hand of cards with a flush (cards of the same suit).
   * @param n The number of cards to deal in the flush hand.
   * @return A list of PlayingCard objects representing the dealt flush hand.
   */
  public List<PlayingCard> dealFlushHand(int n) {
    List<PlayingCard> flushHand = new ArrayList<>();
    if (n < 5 || cards.size() < n) {
      return flushHand;
    }

    char suit = SUITS[random.nextInt(SUITS.length)];
    List<PlayingCard> filteredCards = cards.stream()
      .filter(card -> card.getSuit() == suit)
      .collect(Collectors.toList());

    for (int i = 0; i < n && !filteredCards.isEmpty(); i++) {
      PlayingCard card = filteredCards.remove(random.nextInt(filteredCards.size()));
      flushHand.add(card);
      cards.remove(card);
    }

    return flushHand;
  }

  /**
   * Returns the number of cards left in the deck.
   * @return The number of cards left in the deck.
   */
  public int cardsLeft() {
    return cards.size();
  }
}
