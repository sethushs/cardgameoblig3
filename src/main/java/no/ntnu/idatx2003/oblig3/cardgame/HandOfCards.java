package no.ntnu.idatx2003.oblig3.cardgame;


import java.util.List;
import java.util.stream.Collectors;
/**
 * The HandOfCards class represents a hand of playing cards.
 * It provides methods for checking specific combinations in the hand,
 * calculating the sum of face values, and extracting cards of a certain suit.
 */
public class HandOfCards {

  /** The list of playing cards in the hand. */
  private List<PlayingCard> hand;

  /**
   * Constructs a new hand of cards with the given list of playing cards.
   * @param hand The list of playing cards representing the hand.
   */
  public HandOfCards(List<PlayingCard> hand) {
    this.hand = hand;
  }

  /**
   * Checks if the hand contains a flush (five cards of the same suit).
   * @return true if the hand contains a flush, false otherwise.
   */
  public boolean hasFlush() {
    return hand.stream().collect(Collectors.groupingBy(PlayingCard::getSuit, Collectors.counting()))
      .values().stream().anyMatch(count -> count >= 5);
  }

  /**
   * Checks if the hand contains the Queen of Spades.
   * @return true if the hand contains the Queen of Spades, false otherwise.
   */
  public boolean hasQueenOfSpades() {
    return hand.stream().anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
  }

  /**
   * Calculates the sum of face values of all cards in the hand.
   * @return The sum of face values.
   */
  public int sumOfFaces() {
    return hand.stream().mapToInt(PlayingCard::getFace).sum();
  }

  /**
   * Retrieves the cards of hearts from the hand.
   * @return A string containing the face values of hearts cards in the hand.
   */
  public String cardsOfHearts() {
    return hand.stream().filter(card -> card.getSuit() == 'H')
      .map(PlayingCard::getAsString).collect(Collectors.joining(" "));
  }

  /**
   * Displays the cards in the hand as a comma-separated string.
   * @return A string representation of the cards in the hand.
   */
  public String displayHand() {
    return hand.stream().map(PlayingCard::getAsString).collect(Collectors.joining(", "));
  }

  /**
   * Gets the list of playing cards in the hand.
   * @return The list of playing cards.
   */
  public List<PlayingCard> getHand() {
    return hand;
  }
}
