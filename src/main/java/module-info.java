module com.example.cardgame {
  // Requires the JavaFX graphics module for the UI
  requires javafx.graphics;
  // Requires the JavaFX controls module for UI components
  requires javafx.controls;
  // Requires the JavaFX fxml module if you're using FXML
  requires javafx.fxml;

  // Opens your application package to the JavaFX graphics module
  opens no.ntnu.idatx2003.oblig3.cardgame to javafx.graphics;
}
